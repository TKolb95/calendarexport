package tk.tools.calendarexport.Service;

import net.fortuna.ical4j.model.*;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.model.property.*;
import net.fortuna.ical4j.util.RandomUidGenerator;
import net.fortuna.ical4j.util.UidGenerator;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

@Service
public class ExportService {

	private static final String PRODUCT_ID = "calendarDemo";

	private TimeZone timeZone;

	private UidGenerator uidGenerator;

	public ExportService() {
		uidGenerator = new RandomUidGenerator();
		timeZone = createTimeZone();
	}

	public Calendar getFullCalendar() throws ParseException {
		Calendar calendar = getRawCalendar();
		calendar.getComponents().add(getEvent());
		return calendar;
	}

	public Calendar getRawCalendar() {
		Calendar icsCalendar = new Calendar();
		icsCalendar.getProperties().add(new ProdId(PRODUCT_ID));
		icsCalendar.getProperties().add(Version.VERSION_2_0);
		icsCalendar.getProperties().add(CalScale.GREGORIAN);
		return icsCalendar;
	}


	private VEvent getEvent() throws ParseException {
		String eventTitle = "Demo Title";
		DateFormat format = new SimpleDateFormat("dd.mm.yyyy-hh:mm");
		java.util.Date dStart = format.parse("01.12.2019-10:45");
		DateTime dateTimeStart = new DateTime(dStart, timeZone);

		java.util.Date dEnd = format.parse("02.12.2019-09:15");
		DateTime dateTimeEnd = new DateTime(dEnd, timeZone);

		VEvent vEvent = new VEvent(dateTimeStart, dateTimeEnd, eventTitle);

		vEvent.getProperties().add(timeZone.getVTimeZone().getTimeZoneId());
		vEvent.getProperties().add(new Location("Room 42a"));
		vEvent.getProperties().add(new Description("Simple Description of this event"));

		Uid uid = uidGenerator.generateUid();
		vEvent.getProperties().add(uid);

		return vEvent;
	}

	private TimeZone createTimeZone() {
		TimeZoneRegistry registry = TimeZoneRegistryFactory.getInstance().createRegistry();
		TimeZone timeZone = registry.getTimeZone("Europe/Berlin");
		return timeZone;
	}

}
