package tk.tools.calendarexport.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tk.tools.calendarexport.Service.ExportService;

import java.text.ParseException;

@RestController
@RequestMapping("calendars")
public class ExportController {

	@Autowired
	ExportService exportService;

	// Creates a file includes one event
	@RequestMapping(value = "file", produces = "text/calendar")
	public String getCalendarAsFile() throws ParseException {
		return exportService.getFullCalendar().toString();
	}

	// Endpoint to subscribe to a calendar while receiving event updates
	@RequestMapping(value = "url/{id}", produces = "text/calendar")
	public String getCalendarFromUrl(@PathVariable Long id) throws ParseException {
		return exportService.getFullCalendar().toString();
	}

	// Returns the url to a specific calendar
	// Protocol webcal is useful if you want the user to click on the link
	// get a automatic import into calendar
	@RequestMapping(value = "auto")
	public String getCalendarUrlAsWebcal() {
		return "webcal://localhost:8080/calendars/url/1";
	}
}
